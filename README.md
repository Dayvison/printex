# Printex

[![Version][version_badge]][version]
[![Build Status][build_status]][build]
[![Build Status - Windows][build_status_win]][build_win]


## Installation

Simply install to your project:

```bash
sampctl package install Dayvison/samp-printex
```

Include in your code and begin using the library:

```pawn
#include <printex>
```

## Usage

```pawn
main()
{
    printex("Hello");
    printex("World!\n");
}
```

[version]: http://badge.fury.io/gh/Dayvison%2Fsamp-printex
[version_badge]: https://badge.fury.io/gh/Dayvison%2Fsamp-printex.svg
[build]: https://travis-ci.org/Dayvison/samp-printex
[build_status]: https://travis-ci.org/Dayvison/samp-printex.svg?branch=master
[build_win]: https://ci.appveyor.com/project/Dayvison/samp-printex/branch/master
[build_status_win]: https://ci.appveyor.com/api/projects/status/***/branch/master?svg=true
[download]: https://github.com/Dayvison/samp-printex/releases
