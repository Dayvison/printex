#include "natives.hpp"
#include <stdio.h>
#define CHECK_PARAMS(n) { if (params[0] != (n * sizeof(cell))) { printf("SCRIPT: Bad parameter count (Count is %d, Should be %d): ", params[0] / sizeof(cell), n); return 0; } }

// native printex(const str[]);
AMX_DECLARE_NATIVE(Native::printex)
{
	CHECK_PARAMS(1);

	char* msg;
	amx_StrParam(amx, params[1], msg);
	printf("%s",msg);
	return 0;
}